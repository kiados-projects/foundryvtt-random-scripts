/*
Twilight Sanctuary Script v 1.0
By Kiado
Turns off and on a token lighting effect, and rolls and shows Temporary Hitpoints for a target.
*/
main()

async function main(){
    console.log("Twilight Sanctuary Script");
    console.log("Version 1.0 by Kiado")
    if(canvas.tokens.controlled.length == 0 || canvas.tokens.controlled.length > 1){
        ui.notifications.error("Please select your token!")
    }
    let actor = canvas.tokens.controlled[0].actor;
    let token = canvas.tokens.controlled[0];
    let activeLabel = "";
    let tsActive;
    let tsStatusMsg = "";
    console.log(token);
    //Is the effect already active?  Set its state.
     if(token.data.brightLight == 30){
         tsActive = true;
     } else {
         tsActive = false;
     }
    console.log(tsActive)
    // Dialog Template
    let dialogTemplate = `
    <h2>Twilight Sanctuary</h2>
    <div style="display:flex; flex-direction: column; justify-content: space-evenly;">
        <span style="flex:1">Behold the power of Twilight Sanctuary.</span>
        <span style="flex:2">Use the buttons below to utilize your Twilight Sanctuary.</span>
        <span sytle="flex:3"><ul><li>Select a target before you roll Temporary HP's.</li></ul></span>
    </div>
    `
    if(tsActive == true){
        activeLabel = "Effect: Off";
    } else {
        activeLabel = "Effect: On";
    }
    console.log(tsActive)
    console.log(activeLabel)
    new Dialog({
        title: "Twilight Sanctuary",
        content: dialogTemplate,
        buttons: {
            rollTempHP: {
                label: "Roll Temp HP",
                callback: (html) => {
                            // Get Target
                            let targets = Array.from(game.user.targets);
                            if(targets.length == 0 || targets.length > 1){
                                ui.notifications.error("Please target one token.");
                                return;
                            }
                            let targetActor = targets[0].actor.name;
                let actLevel = actor.data.data.details.level;
                let rollString = "1d6 + "+actLevel;
                let roll = new Roll(rollString).roll();
                console.log(roll);
                let chatContent = `
                    <p>You have rolled ${roll.total} Temporary Hit Points for ${targetActor}</p>
                `;
                ChatMessage.create({
                    speaker: {
                        alias: actor.name
                    },
                    content: chatContent
                })
                }
            },
            turnOffOn: {
                label: activeLabel,
                callback: (html) => {
                    if(tsActive == true){
                            token.update({
                                brightLight: 0,
                                lightColor: "#000000",
                                lightAnimation:
                                             {type: ""}
                            })
                            tsStatusMsg = "<p>Twilight Sanctuary has been de-activated.</p>";
                            ui.notifications.info("Twilight Sanctuary has been removed.");
                            tsActive = false;
                    } else {
                            token.update({
                                brightLight: 30,
                                lightColor: "#800080",
                                lightAnimation:
                                            {type: "fog"}
                            })
                            tsStatusMsg = "<p>Twilight Sanctuary has been activated.</p>";
                            ui.notifications.info("Twilight Sanctuary has been activated.");
                            tsActive = true;
                        }
                        ChatMessage.create({
                            speaker: {
                                alias: actor.name
                            },
                            content: tsStatusMsg
                        })
                    }
                }
            },
        }
    ).render(true);
}
